import { BaseEntity } from './base-entity';

export interface EntitiesList<T extends BaseEntity> {
    items: T[];
}

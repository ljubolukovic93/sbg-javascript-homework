/**
 * Generic entity. Every entity has to extend this class.
 * NOTE: If name isn't present in all entities, leave only ID in this class.
 */
export interface BaseEntity {
    id?: string;
    name: string;
}

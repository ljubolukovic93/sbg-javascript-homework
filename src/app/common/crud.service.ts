import { HttpClient, HttpParams } from '@angular/common/http';
import { BaseEntity } from './base-entity';
import { EntitiesList } from './list-entity';

/**
 * Generic implementation of CRUD operations.
 * Intended to be extended for every Entity.
 */
export abstract class CrudService<T extends BaseEntity> {

    constructor(protected uri: string, protected http: HttpClient) {
    }

    /**
     * Loads all items from the backend. Response is wrapped in EntitiesList.
     * @param filter Dynamically extracts properties and values and appends to HttpParam.
     * @param fieldsString Custom properties to be included in the response. Default ones are id and name.
     */
    findAll(filter: any = null, fieldsString: string = 'id,name') {
        let params1 = new HttpParams();
        if (filter) {
            Object.keys(filter).forEach(key => {
                if (filter[key]) {
                    params1 = params1.append(key, filter[key]);
                }
            });
        }
        if (fieldsString) {
            params1 = params1.append('fields', fieldsString);
        }
        return this.http.get<EntitiesList<T>>(this.uri, {
            params: params1
        });
    }

    /**
     * Finds one entity for ID param.
     * @param id Id of the entity to be loaded.
     * @param fieldsString Custom properties to be included in the response. Default ones are id and name.
     */
    findOne(id: string, fieldsString: string = 'id,name') {
        let params1 = new HttpParams();
        if (fieldsString) {
            params1 = params1.append('fields', fieldsString);
        }
        return this.http.get<T>(this.uri + '/' + id, {
            params: params1
        });
    }

    /**
     * Creates new entity on the server side.
     * @param entity data to be inserted in the DB.
     */
    create(entity: T) {
        return this.http.post<T>(this.uri, entity);
    }

    /**
     * HTTP Patch method to update only part of the entity.
     * Id has to be present in entity.
     * @param entity Entity with subset of fields to be updated.
     */
    update(entity: T) {
        let fields = '';
        Object.keys(entity).forEach(key => {
            if (fields.length > 0) {
                fields += ',' + key;
            } else {
                fields += key;
            }
        });
        let params1 = new HttpParams();
        params1 = params1.append('fields', fields);
        return this.http.patch<T>(this.uri + '/' + entity.id, entity, {
            params: params1
        });
    }

    /**
     * No response data, only HTTP status 200 if operation was successful.
     * @param id of the entity to be removed.
     */
    delete(id: string) {
        return this.http.delete(this.uri + '/' + id);
    }

}

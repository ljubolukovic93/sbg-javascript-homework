import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

    private readonly apiUrl = environment.apiUrl;
    private readonly apiKey = environment.apiKey;

    /**
     * Intercept every HTTP request and appends API KEY.
     * Also adds URL prefix to every API call.
     * @param request previous request
     * @param next next handler to handle new http request
     */
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let newRequest = request;
        if (!request.url.startsWith('.') && !request.url.startsWith('http')) {
            newRequest = request.clone({
                headers: request.headers.set('X-SBG-Auth-Token', this.apiKey),
                url: this.apiUrl + request.url
            });
        }
        return next.handle(newRequest);
    }

}

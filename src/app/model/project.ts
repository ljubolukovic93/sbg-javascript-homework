import { BaseEntity } from 'src/app/common';

export interface Project extends BaseEntity {

    href?: string;
    description?: string;

}

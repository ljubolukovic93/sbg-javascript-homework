import { TaskStatus } from './task-status';
import { BaseEntity } from 'src/app/common';

export interface Task extends BaseEntity {

    href?: string;
    description?: string;
    status?: TaskStatus;
    project?: string;

}

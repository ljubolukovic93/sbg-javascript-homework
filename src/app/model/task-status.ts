export enum TaskStatus {

    DRAFT = 'Draft',
    RUNNING = 'Running',
    QUEUED = 'Queued',
    ABORTED = 'Aborted',
    COMPLETED = 'Completed',
    FAILED = 'Failed'

}

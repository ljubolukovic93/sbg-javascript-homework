import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})
export class LoadingComponent {

  /**
   * Custom message to be displayed below Loading icon.
   */
  @Input() message = 'Loading';

}

import { Component, OnInit } from '@angular/core';
import { TaskService } from './../../services';
import { Router, ActivatedRoute } from '@angular/router';
import { Task } from 'src/app/model';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.css']
})
export class TaskDetailComponent implements OnInit {

  /**
   * Task Id. Fetched from the URL.
   */
  id = '';
  /**
   * Task to be loaded from server, then displayed on the screen.
   */
  private task;
  /**
   * Displays loading component when backend operation is in progress.
   */
  loading = true;
  emptyMessage: string;

  constructor(private taskService: TaskService, private router: Router, private route: ActivatedRoute) { }

  showModal = false;

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.loadTask(this.id);
  }

  /**
   * Loads single task from the server by id.
   * If 404 error occurrs, navigates to NotFound Component.
   * If uknown error occurrs, it will remain on loading screen with error displayed.
   * If success, it will display the task.
   * @param id Id of the task to be loaded.
   */
  loadTask(id: string) {
    this.emptyMessage = 'Loading info for task ' + this.id + '...';
    this.taskService.findOne(id).subscribe(
      (data: Task) => {
        this.loading = false;
        this.task = data;
      },
      err => {
        this.emptyMessage = 'An error has occurred while loading task ' + this.id + ': ';
        if (err instanceof HttpErrorResponse) {
          if (err.error.message) {
            this.emptyMessage += err.error.message;
          }
          if (err.status === 404) {
            of(null).pipe(
              delay(3000)).subscribe(data => {
                this.router.navigate(['404'], { skipLocationChange: true });
              });
          } else {
            this.task = {};
          }
        } else {
          this.task = {};
          this.emptyMessage += 'Unknown Error';
        }
      }
    );
  }

  /**
   * Navigates to TaskList Component.
   */
  navigateBack() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}

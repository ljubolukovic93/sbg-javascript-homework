import { ProjectService } from './../../services/project.service';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Project } from 'src/app/model';

@Component({
  selector: 'app-project-modal',
  templateUrl: './project-modal.component.html',
  styleUrls: ['./project-modal.component.css']
})
/**
 * Component for Project Creation.
 * Used on two pages.
 */
export class ProjectModalComponent {

  project: Project = { name: null };

  /**
   * Emits value when user purposly wants to close dialog, or when operation has finished.
   * If operation was successful, emits true, otherwise false.
   */
  @Output() closeEvent = new EventEmitter();

  /**
   * Input field, so outer component can control show/hide.
   */
  @Input() show = false;

  /**
   * Displays loading component when backend operation is in progress.
   */
  loading = false;

  constructor(private projectService: ProjectService) {
  }

  /**
   * Sends data to server.
   * Has to handle error.
   */
  save() {
    this.loading = true;
    this.projectService.create(this.project).subscribe(
      data => {
        alert('Project ' + data.id + ' has been successfully created!');
        this.project = { name: null };
        this.closeEvent.emit(true);
        this.loading = false;
      },
      err => {
        this.loading = false;
        let message = 'An error has occurred while creating new project: ';
        if (err.error && err.error.message) {
          message += err.error.message;
        } else {
          message += 'Unknown error';
        }
        alert(message);
      }
    );
  }

  /**
   * If Backend operation isn't in progress, closes dialog.
   */
  closeDialog() {
    if (this.loading) {
      return;
    }
    this.project = { name: null };
    this.closeEvent.emit(false);
  }

}

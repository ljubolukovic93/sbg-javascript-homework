import { TaskService } from './../../services/task.service';
import { TaskStatus } from './../../model/task-status';
import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/model/task';
import { Router, ActivatedRoute } from '@angular/router';
import { EntitiesList } from 'src/app/common';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  title = 'Tasks';
  /**
   * Limit options. Defined in Homework.
   */
  limits = [5, 10, 25];
  /**
   * List of Statuses for filtering. Defined in Homework. Could be loaded from the server.
   * Here is list of consts.
   */
  statuses: string[] = [];
  /**
   * Filter object to be passed to CRUD service. Only 2 properties requested, could be expanded.
   */
  filter = { status: null, limit: null };

  /**
   * List of tasks to display.
   */
  tasks: Task[];
  /**
   * Displays loading component when backend operation is in progress.
   */
  loading = true;
  emptyMessage: string;
  showModal = false;

  constructor(private router: Router, protected route: ActivatedRoute, private taskService: TaskService) {
    // tslint:disable-next-line:forin
    for (const status in TaskStatus) {
      this.statuses.push(status);
    }
  }

  ngOnInit() {
    this.loadTasks();
  }

  /**
   * Loads tasks for current Filter from the backend.
   * This method is triggered in onInit and on every Filter change.
   */
  loadTasks() {
    this.loading = true;
    this.emptyMessage = 'Task list is empty. Change search filters or add new tasks. :)';
    this.taskService.findAll(this.filter).subscribe(
      (data: EntitiesList<Task>) => {
        this.tasks = data.items;
        if (!this.tasks) {
          this.tasks = [];
        }
        this.loading = false;
      },
      err => {
        this.tasks = [];
        this.emptyMessage = 'An error has occurred while loading list of tasks: ';
        if (err.error && err.error.message) {
          this.emptyMessage += err.error.message;
        } else {
          this.emptyMessage += 'Unknown error';
        }
        this.loading = false;
      }
    );
  }

  /**
   * Sets both filters to null, and reloads table.
   */
  clearFilter() {
    this.filter = { status: null, limit: null };
    this.loadTasks();
  }

  /**
   * Updates filter object.
   * This is done through method, so loadTasks could be called every time.
   * @param filterField Field to be updated
   * @param filterValue New value
   */
  onFilterChange(filterField: string, filterValue: any) {
    if (filterValue) {
      this.filter[filterField] = filterValue;
    } else {
      this.filter[filterField] = null;
    }
    this.loadTasks();
  }

  /**
   * Displays first Confirm Alert. If users confirms it, calls backend service.
   * @param task Task to be deleted
   * @param $event HTML event. Need to call stopPropagation, so click on <li> isn't triggered.
   */
  deleteTask(task: Task, $event) {
    if (confirm('Are you sure you want to delete task ' + task.name)) {
      this.taskService.delete(task.id).subscribe(
        data => {
          alert('Task ' + task.name + ' has been deleted!');
          this.loadTasks();
        },
        err => {
          let message = 'An error has occurred while deleting task ' + task.name + ': ';
          if (err.error && err.error.message) {
            message += err.error.message;
          } else {
            message += 'Unknown error';
          }
          alert(message);
        }
      );
    }
    $event.stopPropagation();
  }

  /**
   * Navigates to Details page.
   * @param task Task to be displayed in details.
   */
  taskDetails(task: Task) {
    this.router.navigate([task.id], { relativeTo: this.route });
  }

}

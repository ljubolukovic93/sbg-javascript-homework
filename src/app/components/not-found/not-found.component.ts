import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-404',
  templateUrl: './not-found.component.html'
})
export class NotFoundComponent {

  constructor(private router: Router) { }

  navigateBack() {
    this.router.navigate(['tasks']);
  }
}

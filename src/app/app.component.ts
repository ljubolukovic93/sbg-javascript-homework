import { User } from './model';
import { UserService } from './services';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'SevenBridges Homework Ljubo';
  loading = true;
  user;

  emptyMessage: string;

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.loading = false;
    this.loadUser();
  }

  loadUser() {
    this.emptyMessage = null;
    this.loading = true;
    this.userService.getMyInfo().subscribe(
      (data: User) => {
        this.loading = false;
        this.user = data;
      },
      err => {
        this.emptyMessage = 'An error has occurred while loading user info: ';
        if (err.error && err.error.message) {
          this.emptyMessage += err.error.message;
        } else {
          this.emptyMessage += 'Unknown error';
        }
        this.loading = false;
      }
    );
  }
}

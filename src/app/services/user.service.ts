import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { User } from './../model';

@Injectable()
export class UserService {

    private uri = 'user';

    constructor(private http: HttpClient) {
    }

    getMyInfo() {
        let params1 = new HttpParams();
        params1 = params1.append('fields', 'username,first_name,last_name');
        return this.http.get<User>(this.uri, { params: params1 });
    }

}

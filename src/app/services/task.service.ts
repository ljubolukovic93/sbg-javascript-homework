import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CrudService, EntitiesList } from 'src/app/common';
import { Task } from 'src/app/model';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable()
export class TaskService extends CrudService<Task> {

    constructor(http: HttpClient) {
        super('tasks', http);
    }

    findOne(id: string, fieldsString: string = 'id,name,status,project') {
        return super.findOne(id, fieldsString);
    }

    /* For Testing purposes
    findAll(filter: any, filterString: string): Observable<EntitiesList<Task>> {
        const fakeData = {
            items: [
                {
                    id: '1',
                    name: 'Name 1'
                },
                {
                    id: '2',
                    name: 'Name 2'
                },
                {
                    id: '3',
                    name: 'Name 3'
                }
            ]
        };
        return of(fakeData).pipe(delay(2000));
    }*/

}

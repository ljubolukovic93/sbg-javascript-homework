import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CrudService } from 'src/app/common';
import { Project } from 'src/app//model';

@Injectable()
export class ProjectService extends CrudService<Project> {

    constructor(http: HttpClient) {
        super('projects', http);
    }

}

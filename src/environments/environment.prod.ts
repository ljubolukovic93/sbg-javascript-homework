export const environment = {
  production: true,
  // SBG API Key for Prod
  // TODO: Replace before Prod
  apiKey: 'api-key-prod',
  // SBG API URL for Prod
  apiUrl: 'https://cavatica-api.sbgenomics.com/v2/'
};

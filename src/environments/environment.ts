export const environment = {
  production: false,
  // SBG API Key for Development
  apiKey: '23fc2bf537c4455fa4d6e76b59426490',
  // SBG API URL for Development
  apiUrl: 'https://cavatica-api.sbgenomics.com/v2/'
};

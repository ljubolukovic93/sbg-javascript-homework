# SbgHomework

Seven Bridges Genomics Homework
Ljubo Lukovic, hiring proccess. April 2019, Belgrade
Task done in Angular 7.

# Prerequisites
Before you begin, make sure your development environment includes Node.js® and an npm package manager.
Angular CLI is optional.

# Angular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## Setup
Run `npm install` to download and install all required dependencies. After this command is executed, node_modules folder will appear in the project.

## Development server
Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
If you are using Angular CLI, you could do same thing with command `ng serve`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.
